package com.serg.imdbclient.presenter;

import android.content.res.Configuration;
import android.view.View;
import android.view.ViewTreeObserver;

import com.serg.imdbclient.IMDBClientApplication;
import com.serg.imdbclient.LayoutManagerProvider;
import com.serg.imdbclient.R;
import com.serg.imdbclient.di.module.GlideRequests;
import com.serg.imdbclient.model.Movie;
import com.serg.imdbclient.view.IMovieInfoView;

import java.util.Observable;

import javax.inject.Inject;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

public class MovieInfoPresenter implements IMovieInfoPresenter {

    private IMovieInfoView movieInfoView;
    //private LayoutManagerProvider provider;
    @Inject
    GlideRequests glideRequests;

    public MovieInfoPresenter(IMovieInfoView movieInfoView/*, LayoutManagerProvider provider*/) {
        this.movieInfoView = movieInfoView;
        //this.provider = provider;
        glideRequests = IMDBClientApplication.getComponent().getGldie();
    }

    @Override
    public void updateMovieInfo(Movie movie) {
        if (movie.getImdbRating() != null) {
            movieInfoView.getProgressBar().setVisibility(View.INVISIBLE);


            final String poster = movie.getPoster();
            ViewTreeObserver treeObserver = movieInfoView.getPoster().getViewTreeObserver();
            treeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    int size = 0;
                    if (movieInfoView.getPoster().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                        size = movieInfoView.getPoster().getHeight();
                    } else {
                        size = movieInfoView.getPoster().getWidth();
                    }
                    glideRequests.load(poster)
                            .transform(new RoundedCornersTransformation(5, 0))
                            .placeholder(R.drawable.movie_100)
                            .override(size, size)
                            .fitCenter()
                            .into(movieInfoView.getPoster());
                }
            });
            movieInfoView.getMovieTitle().setText(movie.getTitle());
            movieInfoView.getMovieRating().setText("IMDB: " + movie.getImdbRating());
            movieInfoView.getMovieCountry().setText("Country: " + movie.getCountry());
            movieInfoView.getMovieDirector().setText("Director: " + movie.getDirector());
            movieInfoView.getMovieActors().setText(movie.getActors());
            movieInfoView.getMoviePlot().setText(movie.getPlot());
        }
    }

    @Override
    public void update(Observable observable, Object o) {
        if (null != o && o instanceof Movie){
            Movie movie = (Movie) o;
            //album.setState(AlbumState.TRACKS_LOADED);
            updateMovieInfo(movie);
        }
    }
}

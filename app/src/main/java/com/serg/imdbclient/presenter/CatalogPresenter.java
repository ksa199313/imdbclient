package com.serg.imdbclient.presenter;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.serg.imdbclient.IMDBClientApplication;
import com.serg.imdbclient.IMovieLoader;
import com.serg.imdbclient.LayoutManagerProvider;
import com.serg.imdbclient.MovieClicked;
import com.serg.imdbclient.R;
import com.serg.imdbclient.model.Catalog;
import com.serg.imdbclient.model.CatalogState;
import com.serg.imdbclient.model.Movie;
import com.serg.imdbclient.view.ICatalogView;
import com.serg.imdbclient.view.IMovieView;
import com.serg.imdbclient.view.MovieView;

import java.util.List;
import java.util.Observable;

import javax.inject.Inject;

import static com.serg.imdbclient.model.CatalogState.LOADING;
import static com.serg.imdbclient.model.CatalogState.SEARCHING;

public class CatalogPresenter implements ICatalogPresenter {

    private ICatalogView catalogView;
    private MovieClicked movieClicked;
    private IMovieLoader loader;
    @Inject Catalog catalog;
    private final MoviesAdapter adapter;

    public CatalogPresenter(ICatalogView catalogView, MovieClicked movieClicked, IMovieLoader loader) {
        this.catalogView = catalogView;
        this.movieClicked = movieClicked;
        catalog = IMDBClientApplication.getComponent().getCatalog();
        this.loader = loader;
        adapter = new MoviesAdapter(catalog, movieClicked);
    }

    @Override
    public void initCatalogView(final RecyclerView.LayoutManager layoutManager) {
        catalogView.getNothingFound().setVisibility(View.INVISIBLE);
        //RecyclerView.LayoutManager layoutManager = provider.provideLayoutManger();
        catalogView.getMoviesRecyclerView().setLayoutManager(layoutManager);
        //adapter = new MoviesAdapter(catalog, movieClicked);
        catalogView.getMoviesRecyclerView().setAdapter(adapter);
        catalogView.getMoviesRecyclerView().setOnScrollListener(
                new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                        int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                        int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                        Log.d("Intent pres cat: ", "isLoading: "+ (catalog.getState()== LOADING) +
                        " loadedpages: " + catalog.getLoadedPages() + " allpages: "+catalog.getPages());
                        //if (!catalog.isLoading() && !(catalog.getLoadedPages() == catalog.getPages())) {
                        if (!(catalog.getState()== LOADING) && !(catalog.getLoadedPages() == catalog.getPages())) {
                            Log.d("Intent pres cat: ", "viscnt: "+visibleItemCount+
                            " 1stvis: "+firstVisibleItemPosition + " totcnt: "+totalItemCount +
                            "all results: " + catalog.getTotalResults());
                            if (totalItemCount < catalog.getTotalResults() && (visibleItemCount + firstVisibleItemPosition) >= totalItemCount / 2
                                    && firstVisibleItemPosition >= 0 ) {
                                Log.d("Intent pres cat: ", "Load more! ");
                                //catalog.setLoading(true);
                                catalog.setState(LOADING);
                                loader.loadMore();
                            }
                        }
                    }
                }
        );
    }

    @Override
    public void onResume(Catalog catalog) {
//        if (catalog.isSearching())
        if (catalog.getState() == SEARCHING)
            showProgress();
        else
            updateCatalog(catalog);
    }

    @Override
    public void updateCatalog(Catalog catalog) {
        if (catalog.getMovies() != null) {
            if (catalog.getMovies().size() != 0) {
                catalogView.getNothingFound().setVisibility(View.INVISIBLE);
                adapter.setCatalog(catalog);
                adapter.notifyDataSetChanged();
            } else {
                catalogView.getNothingFound().setVisibility(View.VISIBLE);
                catalogView.getMoviesRecyclerView().setVisibility(View.INVISIBLE);
            }
        }
    }


    @Override
    public void update(Observable observable, Object o) {
        if (null != o && o instanceof Catalog){
            Catalog catalog = (Catalog) o;
            Log.d("CatPres", "Update!");
            hideProgress();
            updateCatalog(catalog);
        } else if (null != o && o instanceof CatalogState) {
            CatalogState state = (CatalogState) o;
            adapter.setLoading(state == LOADING);
            //adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void showProgress() {
        catalogView.getMoviesRecyclerView().setVisibility(View.INVISIBLE);
        catalogView.getProgressBar().setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        catalogView.getProgressBar().setVisibility(View.INVISIBLE);
        catalogView.getMoviesRecyclerView().setVisibility(View.VISIBLE);
    }

    @Override
    public void showNoInternetSnack() {
        Snackbar.make(catalogView.getMainLayout(),
                R.string.no_connection,
                Snackbar.LENGTH_SHORT)
                .show();
    }

    private static class MoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        //private Catalog catalog;
        private List<Movie> movies;
        private MovieClicked movieClicked;
        private boolean isLoading;

        public void setLoading(boolean loading) {
            isLoading = loading;
        }

        protected static final int ITEM = 0;
        protected static final int FOOTER = 1;

        public MoviesAdapter(Catalog catalog, MovieClicked movieClicked) {
            //this.catalog = catalog;
            this.movies = catalog.getMovies();
            this.movieClicked = movieClicked;
            //isLoading = catalog.isLoading();
            isLoading = catalog.getState() == LOADING;
        }

        public void setCatalog(Catalog catalog) {
            //this.catalog.getMovies() = catalog.getMovies();
            //this.catalog.setMovies(catalog.getMovies());
            //this.catalog.updateCatalog(catalog);
            this.movies = catalog.getMovies();
        }

        @Override
        public int getItemViewType(int position) {
            return ((position == getItemCount()-1) && isLoading) ? FOOTER : ITEM;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == ITEM){
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.movie_item, parent, false);
                return new MovieView(view,
                        (AppCompatTextView)view.findViewById(R.id.title),
                        (AppCompatTextView)view.findViewById(R.id.year),
                        (AppCompatImageView)view.findViewById(R.id.poster)
                );
            } else {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.loading_item, parent, false);
                return new LoadingViewHolder(
                        view,
                        (ProgressBar) view.findViewById(R.id.loading_item));
            }
            /*View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.movie_item, parent, false);
            return new MovieView(view,
                    (AppCompatTextView)view.findViewById(R.id.title),
                    (AppCompatTextView)view.findViewById(R.id.year),
                    (AppCompatImageView)view.findViewById(R.id.poster)
            );*/
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder.getItemViewType() == ITEM) {
            //if (position < getItemCount() - 1){
                holder.itemView.setTag(R.integer.albumNum, position);
                IMoviePresenter moviePresenter = new MoviePresenter((IMovieView) holder);
                moviePresenter.updateMovie(movies.get(position), movieClicked.showMovieInfo());
                Log.d("bindView", "pos: " + position);
            } /*else {
                if (!catalog.isLoading()) {
                    holder.itemView.setTag(R.integer.albumNum, position);
                    IMoviePresenter moviePresenter = new MoviePresenter((IMovieView) holder);
                    moviePresenter.updateMovie(catalog.getMovies().get(position), movieClicked.showMovieInfo());
                    Log.d("bindView", "pos: " + position);
                }
            }*/
        }

        @Override
        public int getItemCount() {
            if(movies != null)
                return isLoading ?
                        (movies.size() + 1) : movies.size();
            else
                return 0;
        }
    }

    public static class LoadingViewHolder extends RecyclerView.ViewHolder {

        private ProgressBar progressBar;

        public LoadingViewHolder(View itemView, ProgressBar progressBar) {
            super(itemView);
            this.progressBar = progressBar;
        }
    }
}

package com.serg.imdbclient.presenter;

import android.support.v7.widget.RecyclerView;

import com.serg.imdbclient.model.Catalog;

import java.util.Observer;

public interface ICatalogPresenter extends Observer {
    void initCatalogView(RecyclerView.LayoutManager layoutManager);
    void updateCatalog(Catalog catalog);
    void showNoInternetSnack();
    void showProgress();
    void hideProgress();
    void onResume(Catalog catalog);
}

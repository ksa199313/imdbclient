package com.serg.imdbclient.presenter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.serg.imdbclient.IMDBClientApplication;
import com.serg.imdbclient.R;
import com.serg.imdbclient.di.module.GlideRequests;
import com.serg.imdbclient.model.Movie;
import com.serg.imdbclient.view.IMovieView;

import javax.inject.Inject;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

public class MoviePresenter implements IMoviePresenter {

    private IMovieView movieView;
    @Inject
    GlideRequests glideRequests;

    public MoviePresenter(IMovieView movieView) {
        this.movieView = movieView;
        glideRequests = IMDBClientApplication.getComponent().getGldie();
    }

    @Override
    public void updateMovie(Movie movie, View.OnClickListener clickListener) {
        if (movie != null){
            glideRequests.load(movie.getPoster())
                    .transform(new RoundedCornersTransformation(5,0))
                    .thumbnail(0.1f)
                    .placeholder(R.drawable.movie_100)
                    .into(movieView.getPoster());
            movieView.setTitle(movie.getTitle());
            movieView.setYear("Year: " + movie.getYear());
            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) movieView;
            viewHolder.itemView.setOnClickListener(clickListener);
        }
    }
}

package com.serg.imdbclient.presenter;

import com.serg.imdbclient.model.Movie;

import java.util.Observer;

public interface IMovieInfoPresenter extends Observer {
    void updateMovieInfo(Movie movie);
}

package com.serg.imdbclient.presenter;

import android.view.View;
import com.serg.imdbclient.model.Movie;

public interface IMoviePresenter {
    void updateMovie(Movie movie, View.OnClickListener clickListener);
}

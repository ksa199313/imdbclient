package com.serg.imdbclient.model;

public class IMDBRating implements IRating {
    private String supplier;
    private String rating;
    private String imdbId;

    public IMDBRating(String supplier, String rating) {
        this.supplier = supplier;
        this.rating = rating;
        this.imdbId = null;
    }

    @Override
    public String getRatingSupplier() {
        return supplier;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    @Override
    public String getRating() {
        return rating;
    }
}

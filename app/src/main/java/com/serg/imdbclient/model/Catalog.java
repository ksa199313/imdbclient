package com.serg.imdbclient.model;

import java.util.List;
import java.util.Observable;

public class Catalog extends Observable {

    private List<Movie> movies;
    //private boolean isLoading = false;
    private CatalogState state = CatalogState.IDLE;
    //private boolean searching = false;
    private String lastQueue = null;
    private int loadedPages = 1;
    private int totalResults = 0;
    private int pages = 0;
    private int lastPageResults = 0;

    public int getLoadedPages() {
        return loadedPages;
    }

    public int getPages() {
        return pages;
    }

    public int getLastPageResults() {
        return lastPageResults;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
        pages = totalResults / 10;
        lastPageResults = totalResults % 10;
        if (lastPageResults > 0)
            pages++;
    }

    public void setLoadedPages(int loadedPages) {
        this.loadedPages = loadedPages;
    }

    public CatalogState getState() {
        return state;
    }

    public void setState(CatalogState state) {
        this.state = state;
        setChanged();
        notifyObservers(state);
    }

    /*public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
        setChanged();
        notifyObservers(isLoading());
    }*/

    public void updateCatalog(Catalog catalog) {
        this.setMovies(catalog.getMovies());
    }

    /*public boolean isSearching() {
        return searching;
    }

    public void setSearching(boolean searching) {
        this.searching = searching;
        setChanged();
        notifyObservers(isSearching());
    }*/

    public String getLastQueue() {
        return lastQueue;
    }

    public void setLastQueue(String lastQueue) {
        this.lastQueue = lastQueue;
    }

    public Catalog() {}

    public Catalog(List<Movie> movies) {
        this.movies = movies;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
        modified();
    }

    public void addMovies(List<Movie> movies){
        this.movies.addAll(movies);
        modified();
    }

    public void modified(){
        setChanged();
        notifyObservers(this);
    }

    public Movie getMovieByImdb(String imdbId){
        for (Movie m :
                movies) {
            if (m.getImdbId() != null && m.getImdbId().equals(imdbId))
                return m;
        }
        return null;
    }

    public void updateByImdbId(Movie movie){
        for (Movie m :
                movies) {
            if (m.getImdbId() != null && m.getImdbId().equals(movie.getImdbId()))
                m.setMissingFields(movie);
        }
        setChanged();
        notifyObservers(getMovieByImdb(movie.getImdbId()));
    }
}

package com.serg.imdbclient.model;

public class IMDRating implements IRating {
    private String supplier;
    private String rating;

    public IMDRating(String supplier, String rating) {
        this.supplier = supplier;
        this.rating = rating;
    }

    @Override
    public String getRatingSupplier() {
        return supplier;
    }

    @Override
    public String getRating() {
        return rating;
    }
}

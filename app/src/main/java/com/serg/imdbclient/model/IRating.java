package com.serg.imdbclient.model;

public interface IRating {
    String getRatingSupplier();
    //double getNumericRating();
    String getRating();
}

package com.serg.imdbclient.model;

public enum CatalogState {
    IDLE, LOADING, SEARCHING
}

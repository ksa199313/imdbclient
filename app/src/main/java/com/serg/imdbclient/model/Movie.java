package com.serg.imdbclient.model;

import java.util.List;

public class Movie {
    private String title;
    private String poster;
    //private List<IRating> ratings;
    private String imdbRating;

    private String country;
    private String director;
    private String actors;
    private String plot;//descr

    private String year;
    private String imdbId;

    public Movie() {
    }

    public Movie(String title, String poster, /*List<IRating> ratings,*/ String imdbRating, String country, String director, String actors, String plot, String year, String imdbId) {
        this.title = title;
        this.poster = poster;
        //this.ratings = ratings;
        this.imdbRating = imdbRating;
        this.country = country;
        this.director = director;
        this.actors = actors;
        this.plot = plot;
        this.year = year;
        this.imdbId = imdbId;
    }

    public Movie(String title, String poster, String year, String imdbId) {
        this.title = title;
        this.poster = poster;
        this.year = year;
        this.imdbId = imdbId;
    }

    public void setMissingFields(String imdbRating, String country, String director, String actors, String plot) {
        this.imdbRating = imdbRating;
        this.country = country;
        this.director = director;
        this.actors = actors;
        this.plot = plot;
    }

    public void setMissingFields(Movie movie){
        this.imdbRating = movie.getImdbRating();
        this.country = movie.getCountry();
        this.director = movie.getDirector();
        this.actors = movie.getActors();
        this.plot = movie.getPlot();
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

/*    public List<IRating> getRatings() {
        return ratings;
    }

    public void setRatings(List<IRating> ratings) {
        this.ratings = ratings;
    }*/

    public String getImdbRating() {
        return imdbRating;
    }

    public void setImdbRating(String imdbRating) {
        this.imdbRating = imdbRating;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }
}

package com.serg.imdbclient;

import android.support.v7.widget.RecyclerView;

public interface LayoutManagerProvider {
    RecyclerView.LayoutManager provideLayoutManger();
}

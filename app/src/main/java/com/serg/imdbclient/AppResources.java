package com.serg.imdbclient;

public class AppResources {
    public static final String SEARCH_QUERY = "search_query";
    public static final String INTENT_BUNDLE = "bundle";
    public static final String MOVIE_NUMBER = "movieNum";
    public static final String MOVIE_NUMBER_BUNDLE = "movieNumBundle";
    public static final String SERVICE_CMD = "CMD";
    public static final String SERVICE_SEARCH_MOVIE = "SEARCH_MOVIE";
    public static final String SERVICE_GET_INFO = "GET_INFO";

    public static final String API_KEY = "6d7ebec7";
    public static final String BASE_API = "http://www.omdbapi.com/";
    public static final String BASE_API_KEY = BASE_API +  "?apikey=" + API_KEY;
    public static final String SEARCH_APIURL = BASE_API_KEY + "&s=";
    public static final String INFO_APIURL = BASE_API_KEY + "&i=";
}

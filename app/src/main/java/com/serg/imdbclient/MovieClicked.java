package com.serg.imdbclient;

import android.view.View;

public interface MovieClicked {
    View.OnClickListener showMovieInfo();
}

package com.serg.imdbclient.view;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class MovieView extends RecyclerView.ViewHolder implements IMovieView {

    private AppCompatTextView title;
    private AppCompatTextView year;
    private AppCompatImageView poster;

    public MovieView(View itemView, AppCompatTextView title, AppCompatTextView year, AppCompatImageView poster) {
        super(itemView);
        this.title = title;
        this.year = year;
        this.poster = poster;
    }

    @Override
    public void setTitle(String title) {
        this.title.setText(title);
    }

    @Override
    public void setYear(String year) {
        this.year.setText(year);
    }

    @Override
    public AppCompatImageView getPoster() {
        return poster;
    }

    @Override
    public IMovieView getInstance() {
        return this;
    }
}

package com.serg.imdbclient.view;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.widget.ProgressBar;

public class MovieInfoView implements IMovieInfoView {

    private AppCompatImageView poster;
    private AppCompatTextView title;
    private AppCompatTextView rating;
    private AppCompatTextView country;
    private AppCompatTextView director;
    private AppCompatTextView actors;
    private AppCompatTextView plot;
    private ProgressBar progressBar;

    public MovieInfoView(AppCompatImageView poster, AppCompatTextView title, AppCompatTextView rating, AppCompatTextView country, AppCompatTextView director, AppCompatTextView actors, AppCompatTextView plot, ProgressBar progressBar) {
        this.poster = poster;
        this.title = title;
        this.rating = rating;
        this.country = country;
        this.director = director;
        this.actors = actors;
        this.plot = plot;
        this.progressBar = progressBar;
    }

    @Override
    public AppCompatImageView getPoster() {
        return poster;
    }

    @Override
    public AppCompatTextView getMovieTitle() {
        return title;
    }

    @Override
    public AppCompatTextView getMovieRating() {
        return rating;
    }

    @Override
    public AppCompatTextView getMovieCountry() {
        return country;
    }

    @Override
    public AppCompatTextView getMovieDirector() {
        return director;
    }

    @Override
    public AppCompatTextView getMovieActors() {
        return actors;
    }

    @Override
    public AppCompatTextView getMoviePlot() {
        return plot;
    }

    @Override
    public ProgressBar getProgressBar() {
        return progressBar;
    }
}

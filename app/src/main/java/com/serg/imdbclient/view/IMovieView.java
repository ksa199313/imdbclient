package com.serg.imdbclient.view;

import android.support.v7.widget.AppCompatImageView;

public interface IMovieView {
    void setTitle(String title);
    void setYear(String year);
    AppCompatImageView getPoster();
    IMovieView getInstance();
}

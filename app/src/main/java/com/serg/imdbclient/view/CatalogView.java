package com.serg.imdbclient.view;

import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;

public class CatalogView implements ICatalogView {

    private RecyclerView moviesRecyclerView;
    private ProgressBar progressBar;
    private AppCompatTextView nothingFound;
    private CoordinatorLayout mainLayout;

    public CatalogView(RecyclerView moviesRecyclerView, ProgressBar progressBar, AppCompatTextView nothingFound, CoordinatorLayout mainLayout) {
        this.moviesRecyclerView = moviesRecyclerView;
        this.progressBar = progressBar;
        this.nothingFound = nothingFound;
        this.mainLayout = mainLayout;
    }

    @Override
    public RecyclerView getMoviesRecyclerView() {
        return moviesRecyclerView;
    }

    @Override
    public ProgressBar getProgressBar() {
        return progressBar;
    }

    @Override
    public AppCompatTextView getNothingFound() {
        return nothingFound;
    }

    @Override
    public CoordinatorLayout getMainLayout() {
        return mainLayout;
    }
}

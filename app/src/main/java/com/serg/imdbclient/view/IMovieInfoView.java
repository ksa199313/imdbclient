package com.serg.imdbclient.view;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;

public interface IMovieInfoView {
    AppCompatImageView getPoster();
    AppCompatTextView getMovieTitle();
    AppCompatTextView getMovieRating();
    AppCompatTextView getMovieCountry();
    AppCompatTextView getMovieDirector();
    AppCompatTextView getMovieActors();
    AppCompatTextView getMoviePlot();
    ProgressBar getProgressBar();
}

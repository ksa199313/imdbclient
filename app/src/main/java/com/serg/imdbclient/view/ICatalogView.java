package com.serg.imdbclient.view;

import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;

public interface ICatalogView {
    RecyclerView getMoviesRecyclerView();
    ProgressBar getProgressBar();
    AppCompatTextView getNothingFound();
    CoordinatorLayout getMainLayout();
}

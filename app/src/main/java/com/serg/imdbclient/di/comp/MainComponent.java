package com.serg.imdbclient.di.comp;

import com.serg.imdbclient.di.module.GlideRequests;
import com.serg.imdbclient.di.module.MainModule;
import com.serg.imdbclient.model.Catalog;

import dagger.Component;

@Component(
        modules = {
                MainModule.class
        }
)
public interface MainComponent {
        Catalog getCatalog();
        GlideRequests getGldie();
        boolean getConnectionStatus();
}

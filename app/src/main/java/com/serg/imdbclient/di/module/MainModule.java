package com.serg.imdbclient.di.module;

import com.serg.imdbclient.IMDBClientApplication;
import com.serg.imdbclient.model.Catalog;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {
    private IMDBClientApplication app;

    public MainModule(IMDBClientApplication app) {
        this.app = app;
    }

    @Provides
    IMDBClientApplication provideApp(){
        return app;
    }

    @Provides
    Catalog provideCatalog(IMDBClientApplication app){
        return app.getCatalog();
    }

    @Provides
    GlideRequests provideGlide(IMDBClientApplication app) {
        return app.getGlide();
    }

    @Provides
    boolean provideConnectionStatus(IMDBClientApplication app) {
        return app.getConnectionStatus();
    }

}

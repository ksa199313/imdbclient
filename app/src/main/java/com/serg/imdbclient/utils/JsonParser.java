package com.serg.imdbclient.utils;

import com.serg.imdbclient.model.Catalog;
import com.serg.imdbclient.model.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JsonParser {

    public static int parseTotalResults(String responseResult) {
        int total_results = 0;
        if (responseResult != null) {
            try {
                JSONObject response = new JSONObject(responseResult);
                total_results = response.getInt("totalResults");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return total_results;
    }

    public static List<Movie> parseSearch(String responseResult){
        List<Movie> movies = new ArrayList<>();
        int total_results = 0;
        if (responseResult != null){
            try {
                JSONObject response = new JSONObject(responseResult);
                total_results = response.getInt("totalResults");
                JSONArray results = response.getJSONArray("Search");
                for (int i = 0; i < 10; i++) {
                    JSONObject jMovie = results.getJSONObject(i);
                    movies.add(
                            new Movie(
                                    jMovie.getString("Title"),
                                    jMovie.getString("Poster"),
                                    jMovie.getString("Year"),
                                    jMovie.getString("imdbID")
                            )
                    );
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return movies;
    }

    public static Movie parseMovie(String responseResult) {
        JSONObject response = null;
        Movie movie = null;
        if (responseResult != null){
            try {
                response = new JSONObject(responseResult);
                movie = new Movie(
                        response.getString("Title"),
                        response.getString("Poster"),
                        response.getString("imdbRating"),
                        response.getString("Country"),
                        response.getString("Director"),
                        response.getString("Actors"),
                        response.getString("Plot"),
                        response.getString("Year"),
                        response.getString("imdbID")
                );
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return movie;
    }
}

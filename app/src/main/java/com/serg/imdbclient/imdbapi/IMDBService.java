package com.serg.imdbclient.imdbapi;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.serg.imdbclient.AppResources;
import com.serg.imdbclient.IMDBClientApplication;
import com.serg.imdbclient.model.Catalog;
import com.serg.imdbclient.model.CatalogState;
import com.serg.imdbclient.model.IRating;
import com.serg.imdbclient.model.Movie;
import com.serg.imdbclient.utils.JsonParser;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.inject.Inject;

public class IMDBService extends IntentService {

    @Inject
    Catalog catalog;

    public IMDBService() {
        super("IMDBApiService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        catalog = IMDBClientApplication.getComponent().getCatalog();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        if (intent != null && intent.getBundleExtra(AppResources.INTENT_BUNDLE) != null) {
            Bundle bundle = intent.getBundleExtra(AppResources.INTENT_BUNDLE);
            if (bundle.getString(AppResources.SERVICE_CMD)
                    .equals(AppResources.SERVICE_SEARCH_MOVIE)) {
                String query = bundle.getString(AppResources.SEARCH_QUERY);
                //if (!catalog.isLoading()){
                //if (!(catalog.getState() == CatalogState.LOADING)){
                /*if (catalog.getState() == CatalogState.SEARCHING
                        || catalog.getState() == CatalogState.IDLE) {*/
                if (query != null) {
                    Log.d("Intent", "Query!! "+query);
                    String encodedQuery = null;
                    try {
                        encodedQuery = URLEncoder.encode(query, "utf-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    String searchQuery = AppResources.SEARCH_APIURL + encodedQuery;
                    StringRequest stringRequest = new StringRequest(Request.Method.GET,
                            searchQuery,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("Intent", response);
                                    catalog.setMovies(JsonParser.parseSearch(response));
                                    catalog.setTotalResults(JsonParser.parseTotalResults(response));
                                    //catalog.setSearching(false);
                                    catalog.setState(CatalogState.IDLE);
                                }
                            },
                            new Response.ErrorListener(){
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d("Intent", "Fuck");
                                    //catalog.setSearching(false);
                                    catalog.setState(CatalogState.IDLE);
                                }
                            });
                    requestQueue.add(stringRequest);
                } else if (catalog.getState() == CatalogState.LOADING) {
                    int pages = catalog.getLoadedPages();
                    pages++;
                    catalog.setLoadedPages(pages);

                    /*String encodedQuery = null;
                    try {
                        encodedQuery = URLEncoder.encode(query, "utf-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }*/
                    String searchQuery = AppResources.SEARCH_APIURL + catalog.getLastQueue()
                            + "&page=" + pages;
                    StringRequest stringRequest = new StringRequest(Request.Method.GET,
                            searchQuery,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("Intent", response);
                                    //catalog.setLoading(false);
                                    if (catalog.getState() != CatalogState.SEARCHING ||
                                            catalog.getState() != CatalogState.IDLE)
                                    catalog.addMovies(JsonParser.parseSearch(response));
                                    catalog.setState(CatalogState.IDLE);
                                }
                            },
                            new Response.ErrorListener(){
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d("Intent", "Fuck");
                                    catalog.setState(CatalogState.IDLE);
                                }
                            });
                    requestQueue.add(stringRequest);
                }
            } else { //movie info
                final int movieNum = bundle.getInt(AppResources.MOVIE_NUMBER);
                String request = AppResources.INFO_APIURL + catalog.getMovies().get(movieNum).getImdbId();
                StringRequest movieInfo = new StringRequest(
                        Request.Method.GET,
                        request,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                //update movie info
                                Movie movie = JsonParser.parseMovie(response);
                                catalog.updateByImdbId(movie);
                                Log.d("Intent", "Update info!!!!!!!");
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Intent", "error in reqqqqqq!");
                            }
                        }
                );
                requestQueue.add(movieInfo);
            }
        }
    }
}

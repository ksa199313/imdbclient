package com.serg.imdbclient;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.serg.imdbclient.di.comp.DaggerMainComponent;
import com.serg.imdbclient.di.comp.MainComponent;
import com.serg.imdbclient.di.module.GlideApp;
import com.serg.imdbclient.di.module.GlideRequests;
import com.serg.imdbclient.di.module.MainModule;
import com.serg.imdbclient.model.Catalog;

public class IMDBClientApplication extends Application {

    private Catalog catalog = new Catalog();
    private static MainComponent component;
    private GlideRequests glide;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerMainComponent.builder()
                .mainModule(new MainModule(this))
                .build();
        glide = GlideApp.with(this);
    }

    public GlideRequests getGlide() {
        return glide;
    }

    public static MainComponent getComponent() {
        return component;
    }

    public boolean getConnectionStatus(){
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public Catalog getCatalog() {
        return catalog;
    }
}

package com.serg.imdbclient.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;

import com.serg.imdbclient.AppResources;
import com.serg.imdbclient.IMDBClientApplication;
import com.serg.imdbclient.LayoutManagerProvider;
import com.serg.imdbclient.R;
import com.serg.imdbclient.model.Catalog;
import com.serg.imdbclient.presenter.IMovieInfoPresenter;
import com.serg.imdbclient.presenter.MovieInfoPresenter;
import com.serg.imdbclient.view.IMovieInfoView;
import com.serg.imdbclient.view.MovieInfoView;

import javax.inject.Inject;

public class MovieInfoActivity extends AppCompatActivity /*implements LayoutManagerProvider */{

    private IMovieInfoView movieInfoView;
    private IMovieInfoPresenter movieInfoPresenter;
    @Inject Catalog catalog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_info);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        movieInfoView = new MovieInfoView(
                (AppCompatImageView) findViewById(R.id.movie_poster),
                (AppCompatTextView) findViewById(R.id.title),
                (AppCompatTextView) findViewById(R.id.rating),
                (AppCompatTextView) findViewById(R.id.country),
                (AppCompatTextView) findViewById(R.id.director),
                (AppCompatTextView) findViewById(R.id.actors),
                (AppCompatTextView) findViewById(R.id.plot),
                (ProgressBar) findViewById(R.id.progress_bar)
                );
        catalog = IMDBClientApplication.getComponent().getCatalog();
        movieInfoPresenter = new MovieInfoPresenter(
                movieInfoView
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        catalog.addObserver(movieInfoPresenter);
        movieInfoPresenter.updateMovieInfo(
                catalog.getMovies().get(
                        getIntent().getBundleExtra(
                                AppResources.MOVIE_NUMBER_BUNDLE).getInt(AppResources.MOVIE_NUMBER)
                )
        );
    }

    @Override
    protected void onPause() {
        super.onPause();
        catalog.deleteObserver(movieInfoPresenter);
    }

    /*@Override
    public RecyclerView.LayoutManager provideLayoutManger() {
        return new LinearLayoutManager(this);
    }*/
}

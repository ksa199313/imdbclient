package com.serg.imdbclient.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;

import com.serg.imdbclient.AppResources;
import com.serg.imdbclient.IMDBClientApplication;
import com.serg.imdbclient.IMovieLoader;
import com.serg.imdbclient.LayoutManagerProvider;
import com.serg.imdbclient.MovieClicked;
import com.serg.imdbclient.R;
import com.serg.imdbclient.di.module.GlideRequests;
import com.serg.imdbclient.imdbapi.IMDBService;
import com.serg.imdbclient.model.Catalog;
import com.serg.imdbclient.model.CatalogState;
import com.serg.imdbclient.presenter.CatalogPresenter;
import com.serg.imdbclient.presenter.ICatalogPresenter;
import com.serg.imdbclient.view.CatalogView;
import com.serg.imdbclient.view.ICatalogView;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements MovieClicked, IMovieLoader {

    private ICatalogView catalogView;
    private ICatalogPresenter catalogPresenter;
    @Inject Catalog catalog;
    @Inject GlideRequests glideRequests;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.movie_24);
        catalogView = new CatalogView(
                (RecyclerView)findViewById(R.id.movies_recycler),
                (ProgressBar) findViewById(R.id.catalog_progress),
                (AppCompatTextView) findViewById(R.id.not_found),
                (CoordinatorLayout) findViewById(R.id.main_layout)
        );
        catalog = IMDBClientApplication.getComponent().getCatalog();
        glideRequests = IMDBClientApplication.getComponent().getGldie();

        //init presenter
        catalogPresenter = new CatalogPresenter(catalogView, this, this);
        catalogPresenter.initCatalogView(new LinearLayoutManager(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        catalog.addObserver(this.catalogPresenter);
        //catalogPresenter.updateCatalog(catalog);
        catalogPresenter.onResume(catalog);
    }

    @Override
    protected void onPause() {
        super.onPause();
        catalog.deleteObservers();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())
                && IMDBClientApplication.getComponent().getConnectionStatus()){
            Log.d("Intent Activ:", "NEW SEARCH");
            catalog.setLoadedPages(1);
            //catalog.setSearching(true);
            catalog.setState(CatalogState.SEARCHING);
            catalogPresenter.showProgress();
            String query = intent.getStringExtra(SearchManager.QUERY);
            catalog.setLastQueue(query);
            Intent i = new Intent(this, IMDBService.class);
            Bundle bundle = new Bundle();
            bundle.putString(AppResources.SEARCH_QUERY, query);
            bundle.putString(AppResources.SERVICE_CMD, AppResources.SERVICE_SEARCH_MOVIE);
            i.putExtra(AppResources.INTENT_BUNDLE, bundle);
            startService(i);
        } else if (!IMDBClientApplication.getComponent().getConnectionStatus()){
            catalogPresenter.showNoInternetSnack();
        }
    }

    @Override
    public void loadMore() {
        Intent i = new Intent(this, IMDBService.class);
        Bundle bundle = new Bundle();
        //bundle.putString(AppResources.SEARCH_QUERY, catalog.getLastQueue());
        bundle.putString(AppResources.SERVICE_CMD, AppResources.SERVICE_SEARCH_MOVIE);
        i.putExtra(AppResources.INTENT_BUNDLE, bundle);
        startService(i);
    }


    @Override
    public View.OnClickListener showMovieInfo() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (IMDBClientApplication.getComponent().getConnectionStatus()) {
                    //pass info & show movie info
                    int movieNum = (int) view.getTag(R.integer.albumNum);
                    Intent i = new Intent(getApplicationContext(), IMDBService.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(AppResources.MOVIE_NUMBER, movieNum);
                    bundle.putString(AppResources.SERVICE_CMD, AppResources.SERVICE_GET_INFO);
                    i.putExtra(AppResources.INTENT_BUNDLE, bundle);
                    startService(i);

                    Intent showMovieInfo = new Intent(MainActivity.this, MovieInfoActivity.class);
                    Bundle b = new Bundle();
                    b.putInt(AppResources.MOVIE_NUMBER, movieNum);
                    showMovieInfo.putExtra(AppResources.MOVIE_NUMBER_BUNDLE, b);
                    startActivity(showMovieInfo);
                } else {
                    catalogPresenter.showNoInternetSnack();
                }
            }
        };
    }
}
